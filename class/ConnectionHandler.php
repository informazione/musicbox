<?php
/**
 * Gestore della connessione al database, responsabile della connessione e
 * dell'astrazione delle transazioni innestabili.
 */
class ConnectionHandler {

    /**
     * l'istanza statica di questa classe
     */
    private static $_instance;

    /**
     * Connessione al database inizializzata in modo lazy
     * 
     * @var PDO
     */
    private $_db_connections = null;

    /**
     * Costruttore privato per impedire che questa classe venga istanziata
     */
    private function __construct() {
        
    }

    /**
     * Restituisce l'unica istanza di questa classe.
     */
    public static function getInstance() {
        if (!isset(self::$_instance)) {
            $c = __CLASS__;
            self::$_instance = new $c;
        }

        return self::$_instance;
    }

    /**
     * Restituisce la connessione al database.
     */
    public function getDBConnection($dbInstanceName = null) {
        global $db;
        if ($this->_db_connections == null) {
            try {
                $this->_db_connections = new PDO($db->driver . ':host=' . $db->hostname . ';dbname=' . $db->database, $db->username, $db->password);
            } catch (PDOException $e) {
                echo 'Errore di connessione: ' . $e->getMessage();
            }
        }
        return $this->_db_connections;
    }

    /**
     * Impedisce la clonazione di questa classe.
     */
    public function __clone() {
        trigger_error('La clonazione di questo oggetto non e\' permessa', E_USER_ERROR);
    }

}
