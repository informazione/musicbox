<?php

class IndexController {

    public function index() {

        $disks = new Disks();
        $result = $disks->getAll();
        $disks_collection = array();
        foreach($result as $d) {
            $disk = new stdClass();
            $disk->id = $d->getId();
            $disk->title = $d->getTitle();
            $disks_collection[] = $disk;
        }   

        $page = new Templater("../templates/main.tpl.php");
        $page->set("title", "Homepage");  
        $content = new Templater("../templates/views/index/index.tpl.php");
        $content->set('disks',$disks_collection);
        $page->set("content", $content->parse());
        $page->publish();
    }
}

