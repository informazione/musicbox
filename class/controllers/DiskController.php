<?php
/**
 * Description of DiskController
 *
 * @author matteo@magni.me
 */
class DiskController {
    
    public function index() {

        $disks = new Disks();
        
        $result = $disks->getAll();
        $disks_collection = array();
        foreach($result as $d) {
            $disk = new stdClass();
            $disk->id = $d->getId();
            $disk->title = $d->getTitle();
            $disks_collection[] = $disk;
        }

        $page = new Templater("../templates/main.tpl.php");
        $page->set("title", "Disk Page");  
        $page->set('head', '');
        $content = new Templater("../templates/views/disk/index.tpl.php");
        $content->set('disks',$disks_collection);
        $page->set("content", $content->parse());
        $page->publish();
    }

    public function add() {

        //debug
        //print_r($_GET);
        //print_r($_POST);
        $page = new Templater("../templates/main.tpl.php");
        $page->set("title", "Add Disk");  
        $page->set('head', '');

        if (isset($_POST['title'])) {
            $diskTitle = $_POST['title'];
    		$disk = new Disk(null,$diskTitle);
    		$disks = new Disks();
    		$disks->insert($disk);

            $content =  'Hai inserito il disco '.$_POST['title'];
            $page->set("content", $content);
        } else {
            $content = new Templater("../templates/views/disk/add.tpl.php");
            $page->set("content", $content->parse());
        }
        $page->publish();
    }

    public function edit($key, $id) {

        $page = new Templater("../templates/main.tpl.php");
        $page->set("title", "edit Disk");  
        $page->set('head', '');

        if (isset($_POST['title'])) {

            $content =  'Hai aggiornato il disco '.$_POST['title'];
            $page->set("content", $content);
        } else {
            $content = new Templater("../templates/views/disk/edit.tpl.php");
            $page->set("content", $content->parse());
        }
        $page->publish();
    }

}

?>
