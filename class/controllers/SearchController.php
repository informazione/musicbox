<?php

/**
 * Description of SearchController
 *
 * @author matteo@magni.me
 */
class SearchController {

    public function index() {

    	$page = new Templater("../templates/main.tpl.php");
    	$page->set("title", "Search Page");
    	$page->set("head", '');
        
        if (isset($_GET['search'])) { 
    		$disks = new Disks();
    		$foundDisks = $disks->search($_GET['search']);
    		$searchResult = 'Risultato della ricerca per <b>'.$_GET['search'].'</b>:<br/>';
    		foreach($foundDisks as $d) {
    			$searchResult = $searchResult.$d->getTitle();
    			$searchResult = $searchResult.'<br/>';
    		}
    		$content = new Templater("../templates/views/search/index.tpl.php");
    		$content->set("searchResult", $searchResult);
    		$page->set("content", $content->parse());
    	} else {
    		$content = new Templater("../templates/views/search/index.tpl.php");
    		$content->set("searchResult", '');
    		$page->set("content", $content->parse());
    	}
    	$page->publish();
    }

}

?>
