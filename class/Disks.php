<?php
/**
 * Description of Disks
 *
 * @author matteo@magni.me
 */
class Disks {

    private $_table = 'disks';

    public function getAll() {
        $connection = ConnectionHandler::getInstance()->getDBConnection();
        try {
            $sth = $connection->prepare('SELECT * FROM '.$this->_table);
            $sth->execute();
            $disks = $sth->fetchAll(PDO::FETCH_OBJ);
            $disks_collection = array();
            foreach($disks as $d) {
                $disk = new Disk();
                $disk->setId($d->id);
                $disk->setTitle($d->title);
                $disks_collection[]=$disk;
            }

        } catch (PDOException $e) {
            print $e->getMessage();
            die();
        }
        return $disks_collection;

    }    
    
    public function insert($disk) {
        $connection = ConnectionHandler::getInstance()->getDBConnection();
        try {
            $sth = $connection->prepare('INSERT INTO '.$this->_table.' (id, title) VALUES (:id, :title)');
            $id = $disk->getId();
            $title = $disk->getTitle();
            $sth->bindParam(':id', $id, PDO::PARAM_INT);
            $sth->bindParam(':title', $title, PDO::PARAM_STR);
            $sth->execute();
        } catch (PDOException $e) {
            die('Insert failed: ' . $e->getMessage());
        }
    }

    public function search($search) {
        $connection = ConnectionHandler::getInstance()->getDBConnection();
        try {
            $sth = $connection->prepare('SELECT * FROM '.$this->_table.' WHERE title LIKE \'%' . $search . '%\'');
            $sth->execute();
            $result = $sth->fetchAll(PDO::FETCH_OBJ);
            $disks = array();
            foreach ($result as $element) {
                $disk = new Disk($element->id, $element->title);
                $disks[] = $disk;
            }
        } catch (PDOException $e) {
            die('Execution failed: ' . $e->getMessage());
        }
        return $disks;
    }
}

?>
