<?php
/**
 * Description of Disk
 *
 * @author matteo@magni.me
 */
class Disk {
    
    protected $id;
    protected $title;
    
    public function __construct($id=null, $title=null) {
    
        if (!is_null($id))
            $this->id = $id;
        if (!is_null($title))
            $this->title = $title;
    }
    
    public function getTitle() {
        return $this->title;
    }
    
    public function setTitle($title) {
        $this->title = $title;
    }
    
    
    public function getId() {
        return $this->id;
    }
    
    public function setId($id) {
        $this->id = $id;
    }


    public function __get($var)
    {
        $func = 'get'.ucfirst($var);
        print $func;
        //exit();
        if (method_exists($this, $func)) {
            //print "$func()<br/>";
            return $this->$func();
        } else {
            throw new Exception("Inexistent property: $var");
        }
    }

    public function __set($var, $value) {

        //print $var.'<br/>';
        //print $value.'<br/>';
        $func = 'set'.ucfirst($var);
        if (method_exists($this, $func)) {
            $this->$func($value);
            print "$func($value)<br/>";
        } else {
            if (method_exists($this, 'get'.ucfirst($var))) {
                throw new Exception("property $var is read-only");
            } else {
                throw new Exception("Inexistent property: $var");
            }
        }
    }

}


?>
