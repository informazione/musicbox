<?php

require 'config/config.php';

//@TODO
//introdurre spl_autoload_register
function __autoload($className) {
    if (strpos($className,'Controller'))
        require_once 'class/controllers/'.$className.'.php';
    else 
        require_once 'class/'.$className.'.php';
}

/*function __autoload_musicbox($className) {
    if (strpos($className,'Controller'))
        require_once 'class/controllers/'.$className.'.php';
    else 
        require_once 'class/'.$className.'.php';
}*/
//spl_autoload_register('__autoload_musicbox');
//$connection = ConnectionHandler::getInstance()->getDBConnection();

$path = 'library/Zend';
$zend_path = substr($path, 0, -5);
set_include_path(get_include_path() . PATH_SEPARATOR . dirname(__FILE__) . DIRECTORY_SEPARATOR . $zend_path);

//require_once $path . '/Loader/Autoloader.php';
//Zend_Loader_Autoloader::getInstance();

//$loader = new Zend_Loader_Autoloader_Resource(array(
//                    'basePath' => dirname(__FILE__) . '/class/controller',
//                    'namespace' => 'Music',
//                    )); 

//$loader->addResourceType('model', 'class', 'Model');
//$loader->addResourceType('controller', 'ClassController', 'Controller');


require_once $path . '/Validate.php';
require_once $path . '/Registry.php';
Zend_Registry::set('AppName', 'MusicBox');

$frontController = new FrontController();
$frontController->run();
?>
