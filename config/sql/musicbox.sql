--
-- Database: `musicbox`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `disks`
--

CREATE TABLE IF NOT EXISTS `disks` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

